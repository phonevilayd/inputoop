package co.simplon.promo16.Classes;

import java.util.Scanner;

public class NumberGuesser {
    private int answer = 5;
    private boolean over = false;

    public String guess(int userAnswer) {
        
        if (answer < userAnswer) {
            return "C'est moins";

        } else if (answer > userAnswer) {
            return "C'est plus";

        } else {
            return "Félicitations !";
        }

    }

    public void run() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Trouvez la réponse");
        while (!over) {
            
            int result = scanner.nextInt();

            String feedback = guess(result);
            System.out.println(feedback);

    
        }

        scanner.close();
    }
        

}

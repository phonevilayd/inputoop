package co.simplon.promo16.Inheritance;

public class Person implements IExample, IhouseItem {
    

        public String greeting(String name) {
            
            return "Hello, "+name+", I am human";
        }
    
        public String tellName() {
            return "i am jean";
        }
    
        @Override
        public boolean canMove() {
            // TODO Auto-generated method stub
            return false;
        }
    
        @Override
        public void use() {
            // TODO Auto-generated method stub
           greeting("everybody");
        }
    
        @Override
        public String draw() {
            return "🧍";
        }
    
    }
    

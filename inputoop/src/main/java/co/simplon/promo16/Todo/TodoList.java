package co.simplon.promo16.Todo;

import java.util.*;

public class TodoList {

   private List<Task> tasks;
    
// Constructeur facultatif
   public TodoList() {

    this.tasks = new ArrayList<>();
}



public void addTask(String label) {
    Task task = new Task(label);
    tasks.add(task);
   }

public void actionTask(int index) {
    tasks.get(index).toggleDone();
}

public void getList(){
    System.out.println(tasks);
}
    
   public void clearDone (){
       for (int i = 0; i < tasks.size(); i++) {
           if (tasks.get(i).isDone()) {
               tasks.remove(i);
               i--;
           }
       }

   }

   public String display() {
    String todoListString = "";
       for (Task task : tasks) {
        todoListString += task.display() + "\n";

       }
       return todoListString;
   }


}

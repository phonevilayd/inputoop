package co.simplon.promo16.Inheritance;

public class CleaningBot extends Robots {
    private int dustbag = 0;

    public CleaningBot(int battery) {
        super(battery);
    }

    public void clean() {
        if (isOn() == true) {
            dustbag += 5;
            discharge(5);
            System.out.println("la li la lou I'm cleaning the house");

            if (dustbag == 100) {
                System.out.println("I AM FULL");
            }

        } else {
            System.out.println("Is Off");
        }

    }
    @Override
    public String draw() {
        return "🧹" + super.draw();
    }

    @Override
    public void use() {
        clean();
    }
}

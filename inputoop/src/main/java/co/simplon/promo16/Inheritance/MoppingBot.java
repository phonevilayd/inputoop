package co.simplon.promo16.Inheritance;

public class MoppingBot extends CleaningBot {
    public MoppingBot(int battery) {
        super(battery);
    }

    private boolean dryCleaning = true;
    private int waterLevel = 100;

    public void toggleDryCleaning (){
        dryCleaning = !dryCleaning;
    }
    //Redéfinition d'une méthode
    //@Override

    public void clean() {
        if(!dryCleaning && waterLevel >= 5) {
            discharge(5);
            waterLevel -= 10;
            System.out.println("mopping on");
        } else {
            super.clean();
        }
    }



    
}

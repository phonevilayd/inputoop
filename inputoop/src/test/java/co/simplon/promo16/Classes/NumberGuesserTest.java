package co.simplon.promo16.Classes;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class NumberGuesserTest {
    NumberGuesser numberGuesser;

    @Before
    public void init() {

    numberGuesser = new NumberGuesser();

    }

    @Test
    public void shouldSaylesser() {

        assertEquals("C'est moins", numberGuesser.guess(7));

    }

    @Test
    public void shouldSayGreater() {

        assertEquals("C'est plus", numberGuesser.guess(3));
    }

    @Test
    public void congratulations() {

        assertEquals("Félicitations !", numberGuesser.guess(5));

        
    }
}

package co.simplon.promo16.Inheritance;

import java.util.Scanner;

public class HouseScanner {
    private SmartHouse myHouse;
    private Scanner scanner;

    public HouseScanner(SmartHouse myHouse) {
        this.scanner = new Scanner(System.in);
        this.myHouse = myHouse;
    }

    public void start() {
        System.out.println("Select an item");
        System.out.println("Add an item");
        System.out.println("exit");

        while (true) {
            myHouse.displayHouse();
            System.out.println("""
                    What do ?
                    1 Select item
                    2 Add item
                    3 Exit
                    """);

            int input = scanner.nextInt();

            if (input == 1) {
                itemselection();
            }
            if (input == 2) {

            }
            if (input == 3) {
                System.out.println("Switch off. Bye.");
            }
        }

    }

    public void itemselection() {
        System.out.println("Select item");
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        myHouse.select(x, y);
    }

    public void selectMenu() {
        while (true) {
            myHouse.displayHouse();
            System.out.println("""
                    What do ?
                    1 Move
                    2 Use
                    3 Back
                    """);

            int input = scanner.nextInt();

            if (input == 1) {
                System.out.println("Enter destination");
                myHouse.moveSelected(scanner.nextInt(), scanner.nextInt());
            }
            if (input == 2) {

            }
            if (input == 3) {
                System.out.println("Switch off. Bye.");
            }
        }
    }
}

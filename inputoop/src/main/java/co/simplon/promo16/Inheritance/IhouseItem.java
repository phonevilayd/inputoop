package co.simplon.promo16.Inheritance;

public interface IhouseItem {
    
    public void use();

    public boolean canMove();

    String draw ();
    
}

package co.simplon.promo16.Todo;

import java.util.UUID;

public class Task {

    private String uuid;
    private String label;
    private boolean done;

    public Task(String label) {
        this.label = label;
        this.done = done;
        this.uuid = UUID.randomUUID().toString();
    }   

    public void toggleDone() {
        done = !done;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isDone() {
        return done;
    }


    @Override
    public String toString() {
        return uuid + ", label: " + label+ ", done: " + done;
    }

    public String display(){
        if (!done) {
            return "☐" + label;
        }
        return "☒" + label;
    }
    
}
package co.simplon.promo16;

//import java.util.Scanner;

//import co.simplon.promo16.Classes.Guess;
//import co.simplon.promo16.Classes.NumberGuesser;
import co.simplon.promo16.Inheritance.CleaningBot;
import co.simplon.promo16.Inheritance.HouseScanner;
import co.simplon.promo16.Inheritance.IhouseItem;
import co.simplon.promo16.Inheritance.Robots;
//import co.simplon.promo16.Todo.ScannerTodoList;
//import co.simplon.promo16.Todo.Task;
//import co.simplon.promo16.Todo.TodoList;
import co.simplon.promo16.Inheritance.MoppingBot;
import co.simplon.promo16.Inheritance.Person;
import co.simplon.promo16.Inheritance.SmartHouse;

public class App {
    public static void main(String[] args) {

        /*
         * ScannerTodoList scannerTodo = new ScannerTodoList();
         * scannerTodo.start();
         * /*Guess scanner = new Guess();
         * scanner.guessNumber();
         * 
         * 
         * NumberGuesser numberGuesser = new NumberGuesser();
         * System.out.println(numberGuesser.guess(5));
         * 
         * numberGuesser.run();
         */

        /*
         * TodoList newList = new TodoList();
         * newList.addTask("Ehe");
         * newList.addTask("ë");
         * newList.addTask("ô");
         * newList.addTask("binin");
         * newList.getList();
         * 
         * newList.actionTask(1);
         * 
         * newList.getList();
         * newList.display();
         */
        /*
         * CleaningBot cleaningBot = new CleaningBot(50);
         * cleaningBot.clean();
         * 
         * MoppingBot moppingBot = new MoppingBot(100);
         * moppingBot.clean();
         */

        Person person = new Person();
        SmartHouse smartHouse = new SmartHouse();

        smartHouse.placeItem(new Robots(), 2, 2);
        smartHouse.placeItem(new CleaningBot(0), 5, 5);
        smartHouse.placeItem(new Person(), 3, 3);

        smartHouse.select(2, 2);
        smartHouse.moveSelected(4, 4);

       HouseScanner houseScanner = new HouseScanner(smartHouse);

       houseScanner.start();
    }

}

package co.simplon.promo16.Todo;

import java.util.UUID;

public class Uuid {
    
    public String uuidGenerate() {
        UUID uuid = UUID.randomUUID();
        String uuidAString = uuid.toString();
        return "Your UUID is: " + uuidAString;
    }
}

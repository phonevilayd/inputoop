package co.simplon.promo16.Inheritance;

import java.util.UUID;

public class Robots implements IhouseItem {

    private int battery = 100;
    private String serialNumber; 

    public Robots(int battery) {
        this.battery = battery;
    }

    public Robots() {
        this.serialNumber = UUID.randomUUID().toString();
    }

    public int charge(int value) {
        battery += value;
        if (battery < 100) {
            battery = 100;
        }
        return battery;

    
    }
    
    public int getBattery() {
        return battery;
    }

    protected int discharge(int value){
        battery -= value; 
        if (battery < 0) {
            battery = 0;
        }
        System.out.println("Battery level " + battery );
        return battery;
        
    }

    public boolean isOn (){
        return battery > 0;
    }

    @Override
    public void use() {
        discharge(5);
        System.out.println("Robot discharging ... ");
    }

    @Override
    public boolean canMove() {
        return isOn();
    }

    @Override
    public String draw() {
        return "🤖";
    }
    
    
    
}

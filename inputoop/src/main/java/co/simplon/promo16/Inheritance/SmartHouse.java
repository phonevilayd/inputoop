package co.simplon.promo16.Inheritance;

import java.util.ArrayList;
import java.util.List;

public class SmartHouse implements IhouseItem {
    List<IhouseItem> IhouseItem = new ArrayList<>();
    IhouseItem [][] grid = new IhouseItem [10][10];
    private IhouseItem selected = null;
    //private int xSelected;
    //private int ySelected;

    public void useSelected() {
        if (selected != null) {
            selected.use();
        } else {
            System.out.println("Please select an item first !");
        }
        
    }

    public void select (int x, int y) {
        //xSelected = x;
        //ySelected = y;
        if(grid[x][y] != null) {
            selected = grid[x][y];
        }
    }

    public void moveSelected(int x, int y) {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) { // [i] au cas où la  grille fait 10 * 15
                if (grid[i][j] == selected) {
                    grid[i][j] = null;
                }
            }
        }

       
        if (selected != null && grid[x][y] == null && selected.canMove()) {
            grid[x][y] = selected;
        }

        
    }


    public void placeItem(IhouseItem item, int x, int y) {
        if (!IhouseItem.contains(item) && grid[x][y] == null) {//  vérifie si l'emplacement est vide
            grid[x][y] = item; 
            IhouseItem.add(item);
        }
    }
    /* Afficher la maison dans son état actuel
    */
    public void displayHouse(){
        for (IhouseItem[] row : grid) {
            for (IhouseItem item : row) {
                if (item == null) {
                    System.out.print(".");
                } else {
                    System.out.print(item.draw());
                }
                System.out.print(" ");
            }
            System.out.println();
        }

    }

    @Override
    public void use() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean canMove() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String draw() {
        // TODO Auto-generated method stub
        return null;
    }

}
